import numpy as np
import matplotlib.pyplot as plt

NARMS = 10
NPROBS = 2000

class MAB_UCB:
    def __init__(self,numArms = NARMS,numProbs = NPROBS):
        self.numArms = numArms
        self.numProbs = numProbs
        self.qStars = np.random.randn(numProbs,numArms)
        self.optimalArms = np.argmax(self.qStars,axis = 1)
        self.Ns = np.zeros((numProbs,numArms))
        self.Qs = np.zeros((numProbs,numArms))
        self.avgRewards = []
        self.optimalPercentages = []

    def runLoop(self,numIters):
        for i in range(numIters):
            print('Step ' + str(i + 1))
            rTotal = 0
            armChoices = []
            for k in np.arange(self.numProbs):
                if i<self.numArms:
                    arm = i
                else:
                    arm = np.argmax(self.Qs[k,:] + np.sqrt(2*np.log(i)/self.Ns[k,:]))
                armChoices.append(arm)

                self.Ns[k,arm] += 1
                R = np.random.normal(self.qStars[k,arm],1)
                self.Qs[k,arm] += (R - self.Qs[k,arm])/self.Ns[k,arm]
                rTotal += R

            self.avgRewards.append(rTotal/self.numProbs)
            numOptimals = np.where(armChoices == self.optimalArms)[0].shape[0]
            self.optimalPercentages.append(numOptimals*100.0/self.numProbs)

    def run(self,numIters):
        for i in range(numIters):
            print('Step ' + str(i + 1))
            rTotal = 0

            if i<self.numArms:
                # on the first K steps, sample each arm once
                armChoices = np.ones(self.numProbs,dtype = int)*i
            else:
                armChoices = np.argmax(self.Qs + np.sqrt(2*np.log(i)/self.Ns), axis = 1)

            # indices 0 - <number of MAB problems>
            probIndices = range(self.numProbs)
            self.Ns[probIndices,armChoices] += 1
            R = np.random.randn(self.numProbs) + self.qStars[probIndices,armChoices]
            self.Qs[probIndices,armChoices] += (R - self.Qs[probIndices,armChoices])/self.Ns[probIndices,armChoices]


            self.avgRewards.append(np.sum(R)/self.numProbs)
            numOptimals = np.where(armChoices == self.optimalArms)[0].shape[0]
            self.optimalPercentages.append(numOptimals*100.0/self.numProbs)

if __name__ == '__main__':
    ITERS = 5000
    tb = MAB_UCB()
    tb1000arm = MAB_UCB(1000)
    tb.run(ITERS)
    tb1000arm.run(ITERS)

    xaxis = (np.arange(ITERS) + 1).astype(int)
    # plt.plot(xaxis,tb.avgRewards,'g.-')
    plt.plot(xaxis,tb.avgRewards,'g.-',label = '10 arms')
    plt.plot(xaxis,tb1000arm.avgRewards,'b.-',label = '1000 arms')
    plt.ylabel('Average Reward')
    plt.xlabel('Steps')
    plt.legend()
    plt.show()

    # plt.plot(xaxis,tb.optimalPercentages,'g.-')
    plt.plot(xaxis,tb.optimalPercentages,'g.-',label = '10 arms')
    plt.plot(xaxis,tb1000arm.optimalPercentages,'b.-',label = '1000 arms')
    plt.ylabel('Optimal action percentage')
    plt.xlabel('Steps')
    plt.legend()
    plt.show()
