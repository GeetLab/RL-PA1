import numpy as np
import matplotlib.pyplot as plt

NARMS = 10
NPROBS = 2000
ALPHA = 0.4

class MABsoftmax:
    def __init__(self,temp,alpha = ALPHA,numArms = NARMS,numProbs = NPROBS):
        self.numArms = numArms
        self.numProbs = numProbs
        self.temp = temp
        self.alpha = alpha
        self.qStars = np.random.randn(numProbs,numArms)
        self.optimalArms = np.argmax(self.qStars,axis = 1)
        self.N = np.zeros(numProbs)
        self.Q = np.zeros(numProbs)
        self.Hs = np.zeros((numProbs,numArms))
        self.avgRewards = []
        self.optimalPercentages = []

    def run(self,numIters):
        for i in range(numIters):
            print('Step ' + str(i + 1))

            rTotal = 0
            armChoices = []
            for j in range(self.numProbs):
                Hexp = np.exp(self.Hs[j,:]/self.temp)
                pr = Hexp/np.sum(Hexp)
                arm = np.random.choice(self.numArms,p = pr)
                armChoices.append(arm)

                self.N[j] += 1
                R = np.random.normal(self.qStars[j,arm],1)
                rTotal += R
                self.Q[j] += (R - self.Q[j])/self.N[j]

                addend = -self.alpha*(R - self.Q[j])*pr
                addend[arm] = self.alpha*(R - self.Q[j])*(1 - pr[arm])
                self.Hs[j,:] += addend


            self.avgRewards.append(rTotal/self.numProbs)
            numOptimals = np.where(armChoices == self.optimalArms)[0].shape[0]
            self.optimalPercentages.append(numOptimals*100.0/self.numProbs)

if __name__ == '__main__':
    ITERS = 1000
    tb0 = MABsoftmax(1e-6)
    tb1 = MABsoftmax(0.001)
    tb2 = MABsoftmax(100)
    tb3 = MABsoftmax(1e6)
    tb0.run(ITERS)
    tb1.run(ITERS)
    tb2.run(ITERS)
    tb3.run(ITERS)

    xaxis = (np.arange(ITERS) + 1).astype(int)
    plt.plot(xaxis,tb0.avgRewards,'k.-',label='Temp. = 10^(-6)')
    plt.plot(xaxis,tb1.avgRewards,'r.-',label='Temp. = 0.001')
    plt.plot(xaxis,tb2.avgRewards,'b.-',label='Temp. = 100')
    plt.plot(xaxis,tb3.avgRewards,'g.-',label='Temp. = 10^6')
    plt.ylabel('Average Reward')
    plt.xlabel('Steps')
    plt.legend()
    plt.show()

    plt.plot(xaxis,tb0.optimalPercentages,'k.-',label='Temp. = 10^(-6)')
    plt.plot(xaxis,tb1.optimalPercentages,'r.-',label='Temp. = 0.001')
    plt.plot(xaxis,tb2.optimalPercentages,'b.-',label='Temp. = 100')
    plt.plot(xaxis,tb3.optimalPercentages,'g.-',label='Temp. = 10^6')
    plt.ylabel('Optimal action percentage')
    plt.xlabel('Steps')
    plt.legend()
    plt.show()
