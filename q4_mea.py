import numpy as np
import matplotlib.pyplot as plt
import time

NARMS = 10
NPROBS = 100

class MAB_MEA:
    def __init__(self,epsilon,delta,numArms = NARMS,numProbs = NPROBS):
        self.numArms = numArms
        self.numProbs = numProbs
        self.epsilon = epsilon
        self.delta = delta
        self.qStars = np.random.randn(numProbs,numArms)
        self.optimalArms = np.argmax(self.qStars,axis = 1)

    def run(self):
        # initialize parameters
        eps = self.epsilon/4.0
        delta = self.delta/2.0
        sizeOfS = self.numArms
        CompleteS = np.tile(np.arange(self.numArms),self.numProbs).reshape((self.numProbs,sizeOfS))

        meaRound = 1
        while(sizeOfS>1):
            l = int(2.0*np.log2(3/delta)/(eps**2))   # number of times to sample each arm
            print('Round ' + str(meaRound) + ' ,sampling each arm ' + str(l) + ' times.')

            sampledRewards = np.random.randn(self.numProbs,sizeOfS,l) \
                            + self.qStars[np.arange(self.numProbs).reshape(-1,1),CompleteS].reshape(self.numProbs,sizeOfS,1)
            Q = np.mean(sampledRewards,axis = 2)
            median = np.median(Q,axis = 1).reshape(self.numProbs,1)

            toRetain = np.where(Q>=median)

            '''
            If S has even number of elements, exactly half of them will be less than the median
            (ignoring the case where the middle two elements are equal)
            else, floor(|S|/2) will be less, hence floor(|S|/2) + 1 will be retained
            '''
            if sizeOfS%2 == 0:
                sizeOfS /= 2
            else:
                sizeOfS = sizeOfS/2 + 1
            tempCompleteS = CompleteS[toRetain[0],toRetain[1]].reshape(self.numProbs,sizeOfS)

            # updating S,epsilon and delta
            CompleteS = tempCompleteS
            eps *= 3.0/4.0
            delta /= 2.0

            meaRound += 1

        return CompleteS

    def runLoop(self):
        # initialize parameters
        eps = self.epsilon/4.0
        delta = self.delta/2.0
        sizeOfS = self.numArms
        CompleteS = np.tile(np.arange(self.numArms),self.numProbs).reshape((self.numProbs,sizeOfS))

        incomplete = True
        meaRound = 1
        while(incomplete == True):
            incomplete = False
            tempCompleteS = []
            l = int(2.0*np.log2(3/delta)/(eps**2))   # number of times to sample each arm
            print('Round ' + str(meaRound) + ' ,sampling each arm ' + str(l) + ' times.')

            for k in np.arange(self.numProbs):
                sampledRewards = np.random.randn(CompleteS[k].shape[0],l) + self.qStars[k,CompleteS[k]].reshape(-1,1)
                Q = np.mean(sampledRewards,axis = 1)
                median = np.median(Q)
                tempCompleteS.append(CompleteS[k][np.where(Q>=median)[0]])
                if incomplete == False and np.where(Q>=median)[0].shape[0]>1:
                    incomplete = True

            # updating S,epsilon and delta
            CompleteS = np.array(tempCompleteS)
            eps *= 3.0/4.0
            delta /= 2.0

            meaRound += 1

        return CompleteS

if __name__ == '__main__':
    # 10 arms
    testbed = MAB_MEA(0.3,0.1)
    t0 = time.time()
    predictedBestArms = testbed.runLoop().reshape(-1)
    print('It took ' + str(time.time() - t0) + ' seconds.')
    numOptimal = np.where(predictedBestArms == testbed.optimalArms)[0].shape[0]
    print('Percentage optimal : ' + str(numOptimal*100.0/testbed.numProbs))

    diff = testbed.qStars[range(testbed.numProbs),testbed.optimalArms] - testbed.qStars[range(testbed.numProbs),predictedBestArms]
    fractionBeyondEps = np.where(diff>testbed.epsilon)[0].shape[0]/float(testbed.numProbs)
    print('Fraction of q*(a*) - q*(a) > epsilon : ' + str(fractionBeyondEps)
        + ' for delta = ' + str(testbed.delta))

    sampledRewards = np.random.randn(testbed.numProbs) + testbed.qStars[range(testbed.numProbs),predictedBestArms]
    print('Average reward : ' + str(np.mean(sampledRewards)))



    # 1000 arms
    # '''
    testbed2 = MAB_MEA(0.3,0.1,1000)
    t0 = time.time()
    predictedBestArms2 = testbed2.runLoop().reshape(-1)
    print('It took ' + str(time.time() - t0) + ' seconds.')
    numOptimal = np.where(predictedBestArms2 == testbed2.optimalArms)[0].shape[0]
    print('Percentage optimal : ' + str(numOptimal*100.0/testbed2.numProbs))

    diff = testbed2.qStars[range(testbed2.numProbs),testbed2.optimalArms] - testbed2.qStars[range(testbed2.numProbs),predictedBestArms2]
    fractionBeyondEps = np.where(diff>testbed2.epsilon)[0].shape[0]/float(testbed2.numProbs)
    print('Fraction of q*(a*) - q*(a) > epsilon : ' + str(fractionBeyondEps)
        + ' for delta = ' + str(testbed2.delta))

    sampledRewards = np.random.randn(testbed2.numProbs) + testbed2.qStars[range(testbed2.numProbs),predictedBestArms2]
    print('Average reward : ' + str(np.mean(sampledRewards)))
    # '''

    # running the vectorised version
    '''
    t0 = time.time()
    predictedBestArms = testbed.run().astype(int).reshape(-1)
    print('It took ' + str(time.time() - t0) + ' seconds.')
    numOptimal = np.where(predictedBestArms == testbed.optimalArms)[0].shape[0]
    print('Percentage optimal : ' + str(numOptimal*100.0/testbed.numProbs))
    '''
