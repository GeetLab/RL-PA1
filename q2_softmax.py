import numpy as np
import matplotlib.pyplot as plt

NARMS = 10
NPROBS = 2000

class MABsoftmax:
    def __init__(self,temp,numArms = NARMS,numProbs = NPROBS):
        self.numArms = numArms
        self.numProbs = numProbs
        self.temp = temp    # temperature parameter
        self.qStars = np.random.randn(numProbs,numArms)
        self.optimalArms = np.argmax(self.qStars,axis = 1)
        self.Ns = np.zeros((numProbs,numArms))
        self.Qs = np.zeros((numProbs,numArms))
        self.avgRewards = []
        self.optimalPercentages = []

    def run(self,numIters):
        for i in range(numIters):
            print('Step ' + str(i + 1))

            armChoices = []
            rTotal = 0
            for k in np.arange(self.numProbs):
                Qexp = np.exp(self.Qs[k,:]/self.temp)

                # probabilities of picking the arms; softmax
                pr = Qexp/np.sum(Qexp)

                arm = np.random.choice(self.numArms,p = pr)
                armChoices.append(arm)

                self.Ns[k,arm] += 1
                R = np.random.normal(self.qStars[k,arm],1)
                self.Qs[k,arm] += (R - self.Qs[k,arm])/self.Ns[k,arm]
                rTotal += R

            self.avgRewards.append(rTotal/self.numProbs)
            numOptimals = np.where(armChoices == self.optimalArms)[0].shape[0]
            self.optimalPercentages.append(numOptimals*100.0/self.numProbs)

if __name__ == '__main__':
    ITERS = 1000

    # comparison of various temperature values
    '''
    tb0 = MABsoftmax(0.01)
    tb1 = MABsoftmax(0.2)
    tb2 = MABsoftmax(1)
    tb3 = MABsoftmax(1e4)
    tb0.run(ITERS)
    tb1.run(ITERS)
    tb2.run(ITERS)
    tb3.run(ITERS)

    xaxis = (np.arange(ITERS) + 1).astype(int)
    plt.plot(xaxis,tb0.avgRewards,'r.-',label='Temp. = 0.01')
    plt.plot(xaxis,tb1.avgRewards,'g.-',label='Temp. = 0.2')
    plt.plot(xaxis,tb2.avgRewards,'b.-',label='Temp. = 1')
    plt.plot(xaxis,tb3.avgRewards,'k.-',label='Temp. = 10^4')
    plt.ylabel('Average Reward')
    plt.xlabel('Steps')
    plt.legend()
    plt.show()

    plt.plot(xaxis,tb0.optimalPercentages,'r.-',label='Temp. = 0.01')
    plt.plot(xaxis,tb1.optimalPercentages,'g.-',label='Temp. = 0.2')
    plt.plot(xaxis,tb2.optimalPercentages,'b.-',label='Temp. = 1')
    plt.plot(xaxis,tb3.optimalPercentages,'k.-',label='Temp. = 10^4')
    plt.ylabel('Optimal action percentage')
    plt.xlabel('Steps')
    plt.legend()
    plt.show()
    '''

    # comparison of a testbed of MABs with 10 arms vs 1000 arms
    tb10arm = MABsoftmax(0.2)
    tb100arm = MABsoftmax(0.2,1000)
    tb10arm.run(ITERS)
    tb100arm.run(ITERS)

    xaxis = (np.arange(ITERS) + 1).astype(int)
    plt.plot(xaxis,tb10arm.avgRewards,'r.-',label='10 arms')
    plt.plot(xaxis,tb100arm.avgRewards,'b.-',label='1000 arms')
    plt.ylabel('Average Reward')
    plt.xlabel('Steps')
    plt.legend()
    plt.show()

    plt.plot(xaxis,tb10arm.optimalPercentages,'r.-',label='10 arms')
    plt.plot(xaxis,tb100arm.optimalPercentages,'b.-',label='1000 arms')
    plt.ylabel('Optimal action percentage')
    plt.xlabel('Steps')
    plt.legend()
    plt.show()
