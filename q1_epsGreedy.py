import numpy as np
import matplotlib.pyplot as plt

NARMS = 10
NPROBS = 2000

class MABtestBed:
    '''
    A multi-armed bandit testbed as described in the book.
    Uses epsilon-greedy algorithm.
    '''
    def __init__(self,epsilon,numArms = NARMS,numProbs = NPROBS):
        self.numArms = numArms
        self.numProbs = numProbs    # number of MAB problems
        self.epsilon = epsilon
        self.qStars = np.random.randn(numProbs,numArms)
        self.optimalArms = np.argmax(self.qStars,axis = 1)
        self.Ns = np.zeros((numProbs,numArms))
        self.Qs = np.zeros((numProbs,numArms))
        self.avgRewards = []
        self.optimalPercentages = []

    def runLoop(self,numIters):
        for i in range(numIters):
            print('Step ' + str(i + 1))
            if i == 0:
                armChoices = np.random.randint(0,self.numArms,self.numProbs)
            else:
                armChoices = []
                for j in range(self.numProbs):
                    # choosing whether to explore or to exploit
                    exploreExploit = np.random.choice(2,p = [self.epsilon,1 - self.epsilon])
                    if exploreExploit == 0: # explore
                        armChoices.append(np.random.randint(0,self.numArms))
                    else:   # exploit
                        armChoices.append(np.argmax(self.Qs[j,:]))


            rTotal = 0
            for arm,k in zip(armChoices,np.arange(self.numProbs)):
                self.Ns[k,arm] += 1
                R = np.random.normal(self.qStars[k,arm],1)
                self.Qs[k,arm] += (R - self.Qs[k,arm])/self.Ns[k,arm]
                rTotal += R

            self.avgRewards.append(rTotal/self.numProbs)
            numOptimals = np.where(armChoices == self.optimalArms)[0].shape[0]
            self.optimalPercentages.append(numOptimals*100.0/self.numProbs)

    def run(self,numIters):
        for i in range(numIters):
            print('Step ' + str(i + 1))
            if i == 0:
                armChoices = np.random.randint(0,self.numArms,self.numProbs)
            else:
                exploreExploit = np.random.choice(2,self.numProbs,p = [self.epsilon,1 - self.epsilon])
                exploreIndices = np.where(exploreExploit == 0)[0]
                exploitIndices = np.where(exploreExploit == 1)[0]
                armChoices = np.zeros(self.numProbs).astype(int)
                armChoices[exploreIndices] = np.random.randint(0,self.numArms,exploreIndices.shape[0])
                armChoices[exploitIndices] = np.argmax(self.Qs[exploitIndices,:],axis = 1)

            # indices 0 - <number of MAB problems>
            probIndices = range(self.numProbs)
            self.Ns[probIndices,armChoices] += 1

            '''
            Since variance is 1, sampling from normal distributions with different mean values is same as
            sampling from a standard normal distribution and adding the different mean values correspondingly.
            This makes it possible to sample all of them at once without using a loop.
            '''
            R = np.random.randn(self.numProbs) + self.qStars[probIndices,armChoices]

            self.Qs[probIndices,armChoices] += (R - self.Qs[probIndices,armChoices])/self.Ns[probIndices,armChoices]

            self.avgRewards.append(np.sum(R)/self.numProbs)
            numOptimals = np.where(armChoices == self.optimalArms)[0].shape[0]
            self.optimalPercentages.append(numOptimals*100.0/self.numProbs)

if __name__ == '__main__':
    ITERS = 1000

    '''
    tb0 = MABtestBed(0)
    tb001 = MABtestBed(0.01)
    tb01 = MABtestBed(0.1)
    tb0.run(ITERS)
    tb001.run(ITERS)
    tb01.run(ITERS)
    # tb0.runLoop(ITERS)
    # tb001.runLoop(ITERS)
    # tb01.runLoop(ITERS)

    xaxis = (np.arange(ITERS) + 1).astype(int)
    plt.plot(xaxis,tb0.avgRewards,'r.-',label='epsilon = 0')
    plt.plot(xaxis,tb001.avgRewards,'b.-',label='epsilon = 0.01')
    plt.plot(xaxis,tb01.avgRewards,'g.-',label='epsilon = 0.1')
    plt.ylabel('Average Reward')
    plt.xlabel('Steps')
    plt.legend()
    plt.show()

    plt.plot(xaxis,tb0.optimalPercentages,'r.-',label='epsilon = 0')
    plt.plot(xaxis,tb001.optimalPercentages,'b.-',label='epsilon = 0.01')
    plt.plot(xaxis,tb01.optimalPercentages,'g.-',label='epsilon = 0.1')
    plt.ylabel('Optimal action percentage')
    plt.xlabel('Steps')
    plt.legend()
    plt.show()
    '''


    tb10arm = MABtestBed(0.1)
    tb100arm = MABtestBed(0.1,1000)
    tb10arm.run(ITERS)
    tb100arm.run(ITERS)

    xaxis = (np.arange(ITERS) + 1).astype(int)
    plt.plot(xaxis,tb10arm.avgRewards,'r.-',label='10 arms')
    plt.plot(xaxis,tb100arm.avgRewards,'b.-',label='1000 arms')
    plt.ylabel('Average Reward')
    plt.xlabel('Steps')
    plt.legend()
    plt.show()

    plt.plot(xaxis,tb10arm.optimalPercentages,'r.-',label='10 arms')
    plt.plot(xaxis,tb100arm.optimalPercentages,'b.-',label='1000 arms')
    plt.ylabel('Optimal action percentage')
    plt.xlabel('Steps')
    plt.legend()
    plt.show()
